/* CppTools.cpp: API C++ entre TOL y el paquete nlopt (non linear optimization)
   M�s detalles del paquete en http://ab-initio.mit.edu/wiki/index.php/NLopt

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */


//Starts local namebock scope
#define LOCAL_NAMEBLOCK _local_namebtntLock_

#if defined(_MSC_VER)
#  include <win_tolinc.h>
#endif

#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bcommon.h>
#include <tol/tol_boper.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_bdatgra.h>
#include <tol/tol_btxtgra.h>
#include <tol/tol_bmatgra.h>
#include <tol/tol_bcodgra.h>
#include <tol/tol_blanguag.h>
#if defined(HAVE_CINT)
#include <tol/tolCint.h>
#endif
#include <nlopt.h>
#include <nlopt.hpp>
#include <gsl/gsl_sys.h>

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& _local_namebtntLock_ = _local_unb_->Contens();

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function 
DynAPI void* GetDynLibNameBlockNonLinGloOpt()
{
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = _local_unb_->Contens();
  return(copy);
}

#define ERR(cond,msg,ret) \
if(cond) { \
  Error(_MID<<msg); \
  return(ret); \
}

enum MethodType { 
  TARGET = 0, INEQUATION=1, EQUATION =2, INEQUATION_M=3, EQUATION_M =4 };

static const char* MethodTypeName[5] = {
  "TARGET", "INEQUATION", "EQUATION", "INEQUATION_M", "EQUATION_M" };

#define CATCH_NLOPT(contens_) \
catch ( nlopt::forced_stop e ) { \
  Error(_MID+"Halted because of a forced termination.\n"+e.what()); \
  contens_ = nlopt::FORCED_STOP; \
} \
catch ( nlopt::roundoff_limited e )  \
{ \
  Error(_MID+"Halted because roundoff errors limited progress.\n"+e.what()); \
  contens_ = nlopt::ROUNDOFF_LIMITED; \
} \
catch ( std::bad_alloc e ) { \
  Error(_MID+"Ran out of memory (a memory allocation failed).\n"+e.what()); \
  contens_ = nlopt::OUT_OF_MEMORY; \
} \
catch ( std::invalid_argument e ) { \
  Error(_MID+"Invalid arguments.\n"+e.what()); \
  contens_ = nlopt::INVALID_ARGS; \
} \
catch ( std::runtime_error e ) { \
  Error(_MID+"Generic failure.\n"+e.what()); \
  contens_ = nlopt::FAILURE; \
} \
catch ( std::exception e ) { \
  Error(_MID+"Unknown exception.\n"+e.what()); \
  contens_ = -100; \
} \
catch(...) \
{ \
  Error("EXCEPTION: Uncontrolled exception in TOL evaluator"); \
  contens_ = -1000; \
}

class TolNLopt;

//--------------------------------------------------------------------
class TolMethod : BAtom
//--------------------------------------------------------------------
{
public:

  TolNLopt* tolNLopt_;
  BCode* code_;
  BText cint_;
  const BNameBlock* owner_;
  const BUserNameBlock* additionalData_;
  BText name_;
  MethodType type_;
  const char* type_code_;
  int id_;
  int numEval_;

  TolMethod(
    TolNLopt* tolNLopt, 
    MethodType type, 
    int id,
    BCode& code, 
    const BNameBlock* owner, 
    const BUserNameBlock* additionalData
  )
  : tolNLopt_(tolNLopt),
    code_(NULL),
    cint_(""),
    owner_(NULL),
    additionalData_(additionalData),
    name_(""),
    type_(type), 
    type_code_(MethodTypeName[type]),
    id_(id),
    numEval_(0)
  { 
    SetCode(code, owner);
  }
  TolMethod(
    TolNLopt* tolNLopt, 
    MethodType type, 
    int id,
    const BText& cint
  )
  : tolNLopt_(tolNLopt),
    code_(NULL),
    cint_(""),
    owner_(NULL),
    additionalData_(NULL),
    name_(""),
    type_(type), 
    type_code_(MethodTypeName[type]),
    id_(id),
    numEval_(0)
  { 
    SetCint(cint);
  }
  void SetCode(
    BCode& code, 
    const BNameBlock* owner)
  {
    code_ = &code;
    owner_ = owner;
    name_ = code_->Name();
    if(owner_)
    {
      name_ = owner_->Name()+"::"+name_;
    }
  }
  void SetCint(const BText& cint)
  {
    cint_ = cint;
    name_ = cint;
  }

  const BText& Name() const { return(name_); }
  int IncNumEval();

  double tol_fun_eval(const BMatrix<double> &x, BMatrix<double> &grad);
  double cint_fun_eval(const BMatrix<double> &x, BMatrix<double> &grad);
  double eval(const BMatrix<double>& x, BMatrix<double>& grad);
  double tol_fun_eval(const std::vector<double> &x, std::vector<double> &grad);
  double cint_fun_eval(const std::vector<double> &x, std::vector<double> &grad);
  double eval(const std::vector<double> &x, std::vector<double> &grad);

  std::vector<double> tol_fun_eval_m(const BMatrix<double> &x, BMatrix<double> &grad);
  std::vector<double> eval_m(const BMatrix<double>& x, BMatrix<double>& grad);
  std::vector<double> tol_fun_eval_m(const std::vector<double> &x, std::vector<double> &grad);
  std::vector<double> eval_m(const std::vector<double> &x, std::vector<double> &grad);
  void eval_mfunc(unsigned m, double *result,  unsigned n, const double *x, double *gradient, void *func_data);

};

//--------------------------------------------------------------------
class TolNLopt
//--------------------------------------------------------------------
{
public:
  nlopt::opt opt_;
  int sign_;
  BList* methods_;
  BArray<int> id_;
  BArray<int> numEval_;
  int totEval_;
  int verboseEach_;
  TolMethod* target_;
  double currentBestValue_;
  BArray<TolMethod*> inequations_;
  BArray<TolMethod*> equations_;
  BArray<TolMethod*> inequations_m_;
  BArray<TolMethod*> equations_m_;
  TolNLopt(nlopt::algorithm alg, unsigned n)
  : opt_(alg, n),
    sign_(0),
    methods_(NULL),
    id_(3),
    numEval_(3),
    totEval_(0),
    verboseEach_(0),
    target_(NULL),
    inequations_(),
    equations_(),
    inequations_m_(),
    equations_m_(),
    currentBestValue_(BDat::Unknown().Value())
  {
    id_[TARGET] = id_[INEQUATION] = id_[EQUATION] = id_[INEQUATION_M] = id_[EQUATION_M] = 0;
    numEval_[TARGET] = numEval_[INEQUATION] = numEval_[EQUATION] = numEval_[INEQUATION_M] = numEval_[EQUATION_M] = 0;
  }
 ~TolNLopt()
  {
    if(methods_) { DESTROY(methods_); }
  }

  void save_reference(TolMethod* met)
  {
    if(met->type_==TARGET)
    { target_ = met; }
    else if (met->type_==INEQUATION)
    { inequations_.Add(met); }
    else if(met->type_==EQUATION)
    { equations_.Add(met); }
    else if (met->type_==INEQUATION_M)
    { inequations_m_.Add(met); }
    else if(met->type_==EQUATION_M)
    { equations_m_.Add(met); }
  }

  TolMethod* AddMethod(
    MethodType type,
    BCode& code, 
    const BNameBlock* owner, 
    const BUserNameBlock* additionalData=NULL)
  {
    int id = (id_[type])++;
    TolMethod* met = new TolMethod(
      this, type, id, code, owner, additionalData);
    methods_ = Cons((BCore*)met, methods_);
    save_reference(met);
    return(met);
  }
  TolMethod* AddMethod(
    MethodType type,
    const BText& cint)
  {
    int id = (id_[type])++;
    TolMethod* met = new TolMethod(
      this, type, id, cint);
    methods_ = Cons((BCore*)met, methods_);
    save_reference(met);
    return(met);
  }
  TolMethod* AddMethod(
    MethodType type, 
    BSyntaxObject* funArg, 
    BSyntaxObject* ownerArg=NULL, 
    BSyntaxObject* additionalDataArg=NULL)
  {
    TolMethod* met = NULL; 
    if(funArg->Grammar()==GraCode())
    {
      BCode&  code = Code(funArg);
      BNameBlock* owner = NULL;
      BUserNameBlock* additionalData = NULL;
      if(ownerArg)
      {
        owner = &(UNameBlock(ownerArg)->Contens());
      }
      if(additionalDataArg)
      {
        additionalData = UNameBlock(additionalDataArg);
      }
      met = AddMethod(type, code, owner, additionalData); 
    }
    else if(funArg->Grammar()==GraText())
    {
      BText&  cint = Text(funArg);
      met = AddMethod(type, cint); 
    }
    return(met);
  }

};

//--------------------------------------------------------------------
static BDat code_addr( TolNLopt* ptr )
//--------------------------------------------------------------------
{
  BDat addr = 0.0;
  intptr_t i = reinterpret_cast<intptr_t>(ptr);
  addr.PutValue( i );
  return addr;
}

//--------------------------------------------------------------------
static TolNLopt* decode_addr( BDat& addr )
//--------------------------------------------------------------------
{
  intptr_t i = addr.GetValue();
  return (TolNLopt*)(i); 
}

//--------------------------------------------------------------------
void BMat2Vector(const BMat& m, std::vector<double> &v)
//--------------------------------------------------------------------
{
  int i, n = m.Rows();
  v.reserve(n);
  v.clear();
  for(i=0; i<n; i++) { v.push_back( m(i,0).Value() ); }
}

//--------------------------------------------------------------------
void Vector2BMat(const std::vector<double> &v, BMat& m)
//--------------------------------------------------------------------
{
  int i, n = v.size();
  if((m.Rows()!=n)||(m.Columns()!=1)) { m.Alloc(n,1); }
  for(i=0; i<n; i++) { m(i,0) = v[i]; }
}

//--------------------------------------------------------------------
bool HasNan(const BMatrix<double>& v)
//--------------------------------------------------------------------
{
  int i;
  for(i=0; i<v.Rows(); i++) { if (gsl_isnan(v(i,0))) return(true); }
  return(false);
}

//--------------------------------------------------------------------
bool HasNan(const std::vector<double> &v)
//--------------------------------------------------------------------
{
  size_t i;
  for(i=0; i<v.size(); i++) { if (gsl_isnan(v[i])) return(true); }
  return(false);
}


//--------------------------------------------------------------------
  int TolMethod::IncNumEval()
//--------------------------------------------------------------------
{
  tolNLopt_->numEval_[id_]++;
  tolNLopt_->totEval_++;
  return(++numEval_);
}

//--------------------------------------------------------------------
double TolMethod::tol_fun_eval(
  const BMatrix<double> &x, 
        BMatrix<double> &grad)
//--------------------------------------------------------------------
{
  BCode& code = *(BCode*)code_;
  if(owner_)
  {
    code.Operator()->PutNameBlock(owner_);
  } 
 	BUserMat* uX  = new BContensMat;
 	BUserMat* uG  = new BContensMat;
  uG->IncNRefs();
  bool gradNeeded = grad.Rows()!=0;
  BMat& X = uX->Contens();
  BMat& G = uG->Contens();
  X = *(const BMatrix<BDat>*)&x;
  if(gradNeeded) 
  { 
    G = *(BMatrix<BDat>*)&grad; 
  }
  else
  {
    G.Alloc(0,0);
  }
  BList* lst = NULL;
  if(additionalData_)
  {
    ((BAtom*)additionalData_)->IncNRefs();
    lst = Cons(uX, Cons(uG, NCons((BCore*)additionalData_)));
  }
  else
  {
    lst = Cons(uX, NCons(uG));
  }
  BUserDat* uY  = UDat(code.Evaluator(lst));
  BDat r;
	if(uY) 
  { 
    r = Dat(uY);
    if(gradNeeded)
    {
      grad = *(BMatrix<double>*)&G;
    }
  	DESTROY(uY);
    uG->DecNRefs();
  	DESTROY(uG);
    if(additionalData_)
    {
      ((BAtom*)additionalData_)->DecNRefs();
    }
  }
  return(r.Value());
}

//--------------------------------------------------------------------
double TolMethod::tol_fun_eval(
  const std::vector<double> &x, 
        std::vector<double> &grad)
//--------------------------------------------------------------------
{
  BCode& code = *(BCode*)code_;
  if(owner_)
  {
    code.Operator()->PutNameBlock(owner_);
  } 
 	BUserMat* uX  = new BContensMat;
 	BUserMat* uG  = new BContensMat;
  uG->IncNRefs();
  bool gradNeeded = !grad.empty();
  BMat& X = uX->Contens();
  BMat& G = uG->Contens();
  Vector2BMat(x,   X);
  if(gradNeeded) 
  { 
    Vector2BMat(grad,G); 
  }
  else
  {
    G.Alloc(0,0);
  }
  BList* lst = NULL;
  if(additionalData_)
  {
    ((BAtom*)additionalData_)->IncNRefs();
    lst = Cons(uX, Cons(uG, NCons((BCore*)additionalData_)));
  }
  else
  {
    lst = Cons(uX, NCons(uG));
  }
  BUserDat* uY  = UDat(code.Evaluator(lst));
  BDat r;
	if(uY) 
  { 
    r = Dat(uY);
    if(gradNeeded)
    {
      BMat2Vector(G, grad);
    }
  	DESTROY(uY);
    uG->DecNRefs();
  	DESTROY(uG);
    if(additionalData_)
    {
      ((BAtom*)additionalData_)->DecNRefs();
    }

  }
  return(r.Value());
}

//--------------------------------------------------------------------
double TolMethod::cint_fun_eval(
  const std::vector<double> &x, 
        std::vector<double> &grad)
//--------------------------------------------------------------------
{
#if defined(HAVE_CINT)
  const double* x_ptr = &(x[0]);
  double* g_ptr = (grad.size())?&(grad[0]):NULL;
  intptr_t x_ptrNum = (intptr_t)(x_ptr);
  intptr_t g_ptrNum = (intptr_t)(g_ptr);
  BText x_cintArg = BText("((const double*)(")+x_ptrNum+"))";
  BText g_cintArg = BText("((      double*)(")+g_ptrNum+"))";
  BText expression = cint_+"("+x_cintArg+","+g_cintArg+")";
  double result = Cint_double(Cint_calc(expression));
#else
  double result = BDat::Unknown().Value();
#endif
  return(result);
}

//--------------------------------------------------------------------
double TolMethod::cint_fun_eval(
  const BMatrix<double> &x, 
        BMatrix<double> &grad)
//--------------------------------------------------------------------
{
#if defined(HAVE_CINT)
  const double* x_ptr = (const double*)(x.Data().Buffer());
  double* g_ptr = (double*)grad.GetData().GetBuffer();
  intptr_t x_ptrNum = (intptr_t)(x_ptr);
  intptr_t g_ptrNum = (intptr_t)(g_ptr);
  BText x_cintArg = BText("((const double*)(")+x_ptrNum+"))";
  BText g_cintArg = BText("((      double*)(")+g_ptrNum+"))";
  BText expression = cint_+"("+x_cintArg+","+g_cintArg+")";
  double result = Cint_double(Cint_calc(expression));
#else
  double result = BDat::Unknown().Value();
#endif
  return(result);
}

//--------------------------------------------------------------------
double TolMethod::eval(
  const std::vector<double> &x, 
        std::vector<double> &grad)
//--------------------------------------------------------------------
{
  IncNumEval();
  bool hnx = HasNan(x);
  if(hnx) { 
    Error(BText("[nlopt] Cannot handle with unknown values in variable "
    "of function ")+type_code_+" "+name_); }
  if(BGrammar::StopFlag() || hnx) { throw nlopt::forced_stop(); }
  double result = BDat::PosInf();
  if(type_==TARGET) { result *= -tolNLopt_->sign_; }
       if(code_) { result = tol_fun_eval (x, grad); }
  else if(cint_) { result = cint_fun_eval(x, grad); }
  bool hng = HasNan(grad);
  if(hng) { 
    Error(BText("[nlopt] Cannot handle with unknown values in gradient "
    "of function ")+type_code_+" "+name_); }
  bool hnr = gsl_isnan(result)!=0;
  if(hnr) { 
    Error(BText("[nlopt] Cannot handle with unknown values in result "
    "of function ")+type_code_+" "+name_); }
  if(BGrammar::StopFlag() || hng || hnr) { throw nlopt::forced_stop(); }
  int ve = tolNLopt_->verboseEach_;
  if(numEval_==1)
  {
    tolNLopt_->currentBestValue_ = 
     (tolNLopt_->sign_==-1)?BDat::PosInf():BDat::NegInf();
  }
  else if((tolNLopt_->sign_==-1 && result<tolNLopt_->currentBestValue_) ||
          (tolNLopt_->sign_==+1 && result>tolNLopt_->currentBestValue_))
  {
    tolNLopt_->currentBestValue_ = result; 
  }
  if(ve && (type_==TARGET) && !(numEval_ % ve))
  {
    Std(BText("[nlopt] ") + 
     numEval_ + " evaluation. Best target value: "+
     BDat(tolNLopt_->currentBestValue_).Format("%.15lg")+"\n");
  }
  
  return(result); 
}


//--------------------------------------------------------------------
double TolMethod::eval(
  const BMatrix<double> &x, 
        BMatrix<double> &grad)
//--------------------------------------------------------------------
{
  IncNumEval();
  bool hnx = HasNan(x);
  if(hnx) { 
    Error(BText("[nlopt] Cannot handle with unknown values in variable "
    "of function ")+type_code_+" "+name_); }
  if(BGrammar::StopFlag() || hnx) { throw nlopt::forced_stop(); }
  double result = BDat::PosInf();
  if(type_==TARGET) { result *= -tolNLopt_->sign_; }
       if(code_) { result = tol_fun_eval (x,grad); }
  else if(cint_) { result = cint_fun_eval(x,grad); }
  bool hng = HasNan(grad);
  if(hng) { 
    Error(BText("[nlopt] Cannot handle with unknown values in gradient "
    "of function ")+type_code_+" "+name_); }
  bool hnr = gsl_isnan(result)!=0;
  if(hnr) { 
    Error(BText("[nlopt] Cannot handle with unknown values in result "
    "of function ")+type_code_+" "+name_); }
  int ve = tolNLopt_->verboseEach_;
  if(ve && (type_==TARGET) && !(numEval_ % ve))
  {
    Std(BText(" [nlopt] ") + 
     numEval_ + " evaluation of "+
     type_code_+" "+
     name_+" "+BDat(result).Format("%.15lg")+"\n");
  }
  return(result); 
}

//--------------------------------------------------------------------
double usr_fun_eval(
  const std::vector<double> &x, 
        std::vector<double> &grad, 
  void *data)
//--------------------------------------------------------------------
{
  if(!data) { return(BDat::Nan()); }
  TolMethod& met = *(TolMethod*)data;
  return(met.eval(x,grad)); 
}


//--------------------------------------------------------------------
std::vector<double> TolMethod::tol_fun_eval_m(
  const BMatrix<double> &x, 
        BMatrix<double> &grad)
//--------------------------------------------------------------------
{
  BCode& code = *(BCode*)code_;
  if(owner_)
  {
    code.Operator()->PutNameBlock(owner_);
  } 
  BUserMat* uX  = new BContensMat;
  BUserMat* uG  = new BContensMat;
  uG->IncNRefs();
  bool gradNeeded = grad.Rows()!=0;
  BMat& X = uX->Contens();
  BMat& G = uG->Contens();
  X = *(const BMatrix<BDat>*)&x;
  if(gradNeeded) 
  { 
    G = *(BMatrix<BDat>*)&grad; 
  }
  else
  {
    G.Alloc(0,0);
  }
  BList* lst = NULL;
  if(additionalData_)
  {
    ((BAtom*)additionalData_)->IncNRefs();
    lst = Cons(uX, Cons(uG, NCons((BCore*)additionalData_)));
  }
  else
  {
    lst = Cons(uX, NCons(uG));
  }
  BUserMat* uY  = UMat(code.Evaluator(lst));
  std::vector<double> r;
  if(uY) 
  { 
    BMat2Vector(Mat(uY),r);
    if(gradNeeded)
    {
      grad = *(BMatrix<double>*)&G;
    }
    DESTROY(uY);
    uG->DecNRefs();
    DESTROY(uG);
    if(additionalData_)
    {
      ((BAtom*)additionalData_)->DecNRefs();
    }
  }
  return(r);
}

//--------------------------------------------------------------------
std::vector<double> TolMethod::tol_fun_eval_m(
  const std::vector<double> &x, 
        std::vector<double> &grad)
//--------------------------------------------------------------------
{
  BCode& code = *(BCode*)code_;
  if(owner_)
  {
    code.Operator()->PutNameBlock(owner_);
  } 
  BUserMat* uX  = new BContensMat;
  BUserMat* uG  = new BContensMat;
  uG->IncNRefs();
  bool gradNeeded = !grad.empty();
  BMat& X = uX->Contens();
  BMat& G = uG->Contens();
  Vector2BMat(x,   X);
  if(gradNeeded) 
  { 
    Vector2BMat(grad,G); 
  }
  else
  {
    G.Alloc(0,0);
  }
  BList* lst = NULL;
  if(additionalData_)
  {
    ((BAtom*)additionalData_)->IncNRefs();
    lst = Cons(uX, Cons(uG, NCons((BCore*)additionalData_)));
  }
  else
  {
    lst = Cons(uX, NCons(uG));
  }
  BUserMat* uY  = UMat(code.Evaluator(lst));
  std::vector<double> r;
  if(uY) 
  { 
    BMat2Vector(Mat(uY),r);
    if(gradNeeded)
    {
      BMat2Vector(G, grad);
    }
    DESTROY(uY);
    uG->DecNRefs();
    DESTROY(uG);
    if(additionalData_)
    {
      ((BAtom*)additionalData_)->DecNRefs();
    }
  }
  return(r);
}

//--------------------------------------------------------------------
std::vector<double> TolMethod::eval_m(
  const std::vector<double> &x, 
        std::vector<double> &grad)
//--------------------------------------------------------------------
{
  IncNumEval();
  bool hnx = HasNan(x);
  if(hnx) { 
    Error(BText("[nlopt] Cannot handle with unknown values in variable "
    "of function ")+type_code_+" "+name_); }
  if(BGrammar::StopFlag() || hnx) { throw nlopt::forced_stop(); }
  std::vector<double> result;
       if(code_) { result = tol_fun_eval_m (x, grad); }
//else if(cint_) { result = cint_fun_eval_m(x, grad); }
  bool hng = HasNan(grad);
  if(hng) { 
    Error(BText("[nlopt] Cannot handle with unknown values in gradient "
    "of function ")+type_code_+" "+name_); }
  bool hnr = HasNan(result)!=0;
  if(hnr) { 
    Error(BText("[nlopt] Cannot handle with unknown values in result "
    "of function ")+type_code_+" "+name_); }
  if(BGrammar::StopFlag() || hng || hnr) { throw nlopt::forced_stop(); } 
  return(result); 
}


//--------------------------------------------------------------------
std::vector<double> TolMethod::eval_m(
  const BMatrix<double> &x, 
        BMatrix<double> &grad)
//--------------------------------------------------------------------
{
  IncNumEval();
  bool hnx = HasNan(x);
  if(hnx) { 
    Error(BText("[nlopt] Cannot handle with unknown values in variable "
    "of function ")+type_code_+" "+name_); }
  if(BGrammar::StopFlag() || hnx) { throw nlopt::forced_stop(); }
  std::vector<double> result;
       if(code_) { result = tol_fun_eval_m (x,grad); }
//else if(cint_) { result = cint_fun_eval_m(x,grad); }
  bool hng = HasNan(grad);
  if(hng) { 
    Error(BText("[nlopt] Cannot handle with unknown values in gradient "
    "of function ")+type_code_+" "+name_); }
  bool hnr = HasNan(result)!=0;
  if(hnr) { 
    Error(BText("[nlopt] Cannot handle with unknown values in result "
    "of function ")+type_code_+" "+name_); }
  return(result); 
}


//--------------------------------------------------------------------
void usr_fun_eval_m(
  unsigned m, double *result,  unsigned n, 
  const double *x, double *gradient, void *func_data)
//--------------------------------------------------------------------
{
  if(!func_data) { return; }
  TolMethod& met = *(TolMethod*)func_data;
  BMatrix<double> X(n,1,x); 
  BMatrix<double> G(n,1,gradient); 
  std::vector<double> r = met.eval_m(X,G);
  int i;
  for(i=0; i<m; i++) { result[i] = r[i]; }
}


//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_create);
DefMethod(1, BDat_create, 
  "create", 2, 2, "Real Real",
  "(Real id_algorithm, Real n)",
  "Creates an internal C++ instance of nlopt::opt to find global "
  "optimal values of functions R^n->R using the specific algorithm:\n"
  "  GN_DIRECT = 0 \n"
  "  GN_DIRECT_L = 1 \n"
  "  GN_DIRECT_L_RAND = 2 \n"
  "  GN_DIRECT_NOSCAL = 3 \n"
  "  GN_DIRECT_L_NOSCAL = 4 \n"
  "  GN_DIRECT_L_RAND_NOSCAL = 5 \n"
  "  GN_ORIG_DIRECT = 6 \n"
  "  GN_ORIG_DIRECT_L = 7 \n"
  "  GD_STOGO = 8 \n"
  "  GD_STOGO_RAND = 9 \n"
  "  LD_LBFGS_NOCEDAL = 10 \n"
  "  LD_LBFGS = 11 \n"
  "  LN_PRAXIS = 12 \n"
  "  LD_VAR1 = 13 \n"
  "  LD_VAR2 = 14 \n"
  "  LD_TNEWTON = 15 \n"
  "  LD_TNEWTON_RESTART = 16 \n"
  "  LD_TNEWTON_PRECOND = 17 \n"
  "  LD_TNEWTON_PRECOND_RESTART = 18 \n"
  "  GN_CRS2_LM = 19 \n"
  "  GN_MLSL = 20 \n"
  "  GD_MLSL = 21 \n"
  "  GN_MLSL_LDS = 22 \n"
  "  GD_MLSL_LDS = 23 \n"
  "  LD_MMA = 24 \n"
  "  LN_COBYLA = 25 \n"
  "  LN_NEWUOA = 26 \n"
  "  LN_NEWUOA_BOUND = 27 \n"
  "  LN_NELDERMEAD = 28 \n"
  "  LN_SBPLX = 29 \n"
  "  LN_AUGLAG = 30 \n"
  "  LD_AUGLAG = 31 \n"
  "  LN_AUGLAG_EQ = 32 \n"
  "  LD_AUGLAG_EQ = 33 \n"
  "  LN_BOBYQA = 34 \n"
  "  GN_ISRES = 35 \n"
  "  AUGLAG = 36 \n"
  "  AUGLAG_EQ = 37 \n"
  "  G_MLSL = 38 \n"
  "  G_MLSL_LDS = 39 \n"
  "  LD_SLSQP = 40 \n"
  "  LD_CCSAQ = 41 \n",
  BOperClassify::MatrixAlgebra_);
void BDat_create::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::create] ";
  nlopt::algorithm id_algorithm = (nlopt::algorithm)(int)Real(Arg(1));
  int n = (int)Real(Arg(2));
  try {
    TolNLopt* instance = new TolNLopt(id_algorithm, n);
    contens_ = code_addr(instance);
  } CATCH_NLOPT(contens_);
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_destroy);
DefMethod(1, BDat_destroy, 
  "destroy", 1, 1, "Real",
  "(Real _handler)",
  "Destroys an internal C++ instance of nlopt::opt",
  BOperClassify::MatrixAlgebra_);
void BDat_destroy::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::destroy] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  contens_ = instance!=NULL;
  if(instance) { delete instance; }
};


//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_set_min_objective);
DefMethod(1, BDat_set_min_objective, 
  "set_min_objective", 2, 4, 
  "Real {Code|Text} NameBlock NameBlock",
  "(Real _handler, {Code|Text} f "
  "[, NameBlock owner, NameBlock additionalData])",
  "Sets the target function to be minimized",
  BOperClassify::MatrixAlgebra_);
void BDat_set_min_objective::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::set_min_objective] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  contens_ = instance!=NULL;
  if(!instance) { return; }
  instance->sign_ = -1;
  TolMethod* met = instance->AddMethod(TARGET,Arg(2),Arg(3)); 
  try {
    instance->opt_.set_min_objective(usr_fun_eval, met); 
  } CATCH_NLOPT(contens_);
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_set_max_objective);
DefMethod(1, BDat_set_max_objective, 
  "set_max_objective",2, 4, 
  "Real {Code|Text} NameBlock NameBlock",
  "(Real _handler, {Code|Text} f "
  "[, NameBlock owner, NameBlock additionalData])",
  "Sets the target function to be maximized",
  BOperClassify::MatrixAlgebra_);
void BDat_set_max_objective::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::set_max_objective] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  contens_ = instance!=NULL;
  if(!instance) { return; }
  instance->sign_ = +1;
  TolMethod* met = instance->AddMethod(TARGET,Arg(2),Arg(3),Arg(4)); 
  try {
    instance->opt_.set_max_objective(usr_fun_eval, met); 
  } CATCH_NLOPT(contens_);
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_set_lower_bounds);
DefMethod(1, BDat_set_lower_bounds, 
  "set_lower_bounds", 2, 2, "Real Matrix",
  "(Real _handler, Matrix lb)",
  "Sets the lower bounds of variables",
  BOperClassify::MatrixAlgebra_);
void BDat_set_lower_bounds::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::set_lower_bounds] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  contens_ = instance!=NULL;
  if(!instance) { return; }
  BMat& LB = Mat(Arg(2));
  std::vector<double> lb;
  BMat2Vector(LB,lb);
  try {
    instance->opt_.set_lower_bounds(lb); 
  } CATCH_NLOPT(contens_);
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_set_upper_bounds);
DefMethod(1, BDat_set_upper_bounds, 
  "set_upper_bounds", 2, 2, "Real Matrix",
  "(Real _handler, Matrix ub)",
  "Sets the upper bounds of variables",
  BOperClassify::MatrixAlgebra_);
void BDat_set_upper_bounds::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::set_upper_bounds] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  contens_ = instance!=NULL;
  if(!instance) { return; }
  BMat& UB = Mat(Arg(2));
  std::vector<double> ub;
  BMat2Vector(UB,ub);
  try {
    instance->opt_.set_upper_bounds(ub); 
  } CATCH_NLOPT(contens_);
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_add_inequality_constraint);
DefMethod(1, BDat_add_inequality_constraint, 
  "add_inequality_constraint", 2, 5, 
  "Real Real {Code|Text} NameBlock NameBlock",
  "(Real _handler, Real tol, {Code|Text} g "
   "[, NameBlock owner, NameBlock additionalData])",
  "Adds an inequality constraint",
  BOperClassify::MatrixAlgebra_);
void BDat_add_inequality_constraint::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::add_inequality_constraint] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  contens_ = instance!=NULL;
  if(!instance) { return; }
  double tol = Real(Arg(2));
  TolMethod* met = instance->AddMethod(INEQUATION,Arg(3),Arg(4),Arg(5)); 
  try {
    instance->opt_.add_inequality_constraint(usr_fun_eval, met, tol); 
  } 
  CATCH_NLOPT(contens_);
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_add_equality_constraint);
DefMethod(1, BDat_add_equality_constraint, 
  "add_equality_constraint", 2, 5, 
  "Real Real {Code|Text} NameBlock NameBlock",
  "(Real _handler, Real tol, {Code|Text} g "
  "[, NameBlock owner, NameBlock additionalData]])",
  "Adds an equality constraint",
  BOperClassify::MatrixAlgebra_);
void BDat_add_equality_constraint::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::add_equality_constraint] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  contens_ = instance!=NULL;
  if(!instance) { return; }
  double tol = Real(Arg(2));
  TolMethod* met = instance->AddMethod(EQUATION,Arg(3),Arg(4),Arg(5)); 
  try {
    instance->opt_.add_equality_constraint(usr_fun_eval, met, tol); 
  } 
  CATCH_NLOPT(contens_);
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_add_inequality_m_constraint);
DefMethod(1, BDat_add_inequality_m_constraint, 
  "add_inequality_m_constraint", 2, 5, 
  "Real Matrix {Code|Text} NameBlock NameBlock",
  "(Real _handler, Matrix tol, {Code|Text} g "
   "[, NameBlock owner, NameBlock additionalData])",
  "Adds a multiple inequality constraint",
  BOperClassify::MatrixAlgebra_);
void BDat_add_inequality_m_constraint::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::add_inequality_m_constraint] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  contens_ = instance!=NULL;
  if(!instance) { return; }
  BMat& tol_ = Mat(Arg(2));
  vector<double> tol;
  BMat2Vector(tol_,tol);
  TolMethod* met = instance->AddMethod(INEQUATION_M,Arg(3),Arg(4),Arg(5)); 
  try {
    instance->opt_.add_inequality_mconstraint(usr_fun_eval_m, met, tol); 
  } 
  CATCH_NLOPT(contens_);
};


//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_add_equality_m_constraint);
DefMethod(1, BDat_add_equality_m_constraint, 
  "add_equality_m_constraint", 2, 5, 
  "Real Matrix {Code|Text} NameBlock NameBlock",
  "(Real _handler, Matrix tol, {Code|Text} g "
   "[, NameBlock owner, NameBlock additionalData])",
  "Adds a multiple equality constraint",
  BOperClassify::MatrixAlgebra_);
void BDat_add_equality_m_constraint::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::add_equality_m_constraint] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  contens_ = instance!=NULL;
  if(!instance) { return; }
  BMat& tol_ = Mat(Arg(2));
  vector<double> tol;
  BMat2Vector(tol_,tol);
  TolMethod* met = instance->AddMethod(EQUATION_M,Arg(3),Arg(4),Arg(5)); 
  try {
    instance->opt_.add_equality_mconstraint(usr_fun_eval_m, met, tol); 
  } 
  CATCH_NLOPT(contens_);
};



//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_set_local_optimizer);
DefMethod(1, BDat_set_local_optimizer, 
  "set_subsidiary_optimizer", 2, 2, "Real Real",
  "(Real _handler, Real _subsidiary_optimizer)",
  "Some of the algorithms, especially MLSL and AUGLAG, use a different "
  "optimization algorithm as a subroutine, typically for local "
  "optimization and augmented lagrangian. You can change the subsidiary search algorithm and its "
  "tolerances by calling this function.\n"
  "Here, subsidiary_opt is another nlopt::opt object whose parameters are "
  "used to determine the subsidiary search algorithm, its stopping "
  "criteria, and other algorithm parameters. (However, the "
  "objective function, bounds, and nonlinear-constraint parameters "
  "of subsidiary_opt are ignored.) The dimension n of subsidiary_opt must match "
  "that of opt.\n"
  "This function makes a copy of the subsidiary_opt object, so you can "
  "freely destroy your original subsidiary_opt afterwards.",
  BOperClassify::MatrixAlgebra_);
void BDat_set_local_optimizer::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::set_subsidiary_optimizer] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  BDat& _subsidiary_optimizer = Dat(Arg(1));
  TolNLopt* subsidiary = decode_addr(_subsidiary_optimizer);
  contens_ = instance!=NULL;
  if(instance) { 
    try {
    instance->opt_.set_local_optimizer(subsidiary->opt_); 
    } CATCH_NLOPT(contens_);
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_set_stopval);
DefMethod(1, BDat_set_stopval, 
  "set_stopval", 2, 2, "Real Real",
  "(Real _handler, Real stopval)",
  "Stop when an objective value of at least stopval is found.",
  BOperClassify::MatrixAlgebra_);
void BDat_set_stopval::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::set_stopval] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  double stopval = Real(Arg(2));
  contens_ = instance!=NULL;
  if(instance) { 
    try {
    instance->opt_.set_stopval(stopval); 
    } CATCH_NLOPT(contens_);
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_set_ftol_rel);
DefMethod(1, BDat_set_ftol_rel, 
  "set_ftol_rel", 2, 2, "Real Real",
  "(Real _handler, Real tol)",
  "Set relative tolerance on function value.",
  BOperClassify::MatrixAlgebra_);
void BDat_set_ftol_rel::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::set_ftol_rel] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  double tol = Real(Arg(2));
  contens_ = instance!=NULL;
  if(instance) 
  { 
    try {
    instance->opt_.set_ftol_rel(tol); 
    } CATCH_NLOPT(contens_);
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_set_ftol_abs);
DefMethod(1, BDat_set_ftol_abs, 
  "set_ftol_abs", 2, 2, "Real Real",
  "(Real _handler, Real tol)",
  "Set absolute tolerance on function value.",
  BOperClassify::MatrixAlgebra_);
void BDat_set_ftol_abs::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::set_ftol_abs] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  double tol = Real(Arg(2));
  contens_ = instance!=NULL;
  if(instance) 
  { 
    try {
    instance->opt_.set_ftol_abs(tol); 
    } CATCH_NLOPT(contens_);
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_set_xtol_rel);
DefMethod(1, BDat_set_xtol_rel, 
  "set_xtol_rel", 2, 2, "Real Real",
  "(Real _handler, Real tol)",
  "Set relative tolerance on optimization parameters.",
  BOperClassify::MatrixAlgebra_);
void BDat_set_xtol_rel::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::set_xtol_rel] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  double tol = Real(Arg(2));
  contens_ = instance!=NULL;
  if(instance) { 
    try {
    instance->opt_.set_xtol_rel(tol); 
    } CATCH_NLOPT(contens_);
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_set_xtol_abs);
DefMethod(1, BDat_set_xtol_abs, 
  "set_xtol_abs", 2, 2, "Real Matrix",
  "(Real _handler, Matrix tol)",
  "Set absolute tolerances on optimization parameters",
  BOperClassify::MatrixAlgebra_);
void BDat_set_xtol_abs::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::set_xtol_abs] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  BMat& TOL = Mat(Arg(2));
  std::vector<double> tol;
  BMat2Vector(TOL,tol);
  contens_ = instance!=NULL;
  if(instance) { 
    try {
    instance->opt_.set_xtol_abs(tol); 
    } CATCH_NLOPT(contens_);
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_set_maxeval);
DefMethod(1, BDat_set_maxeval, 
  "set_maxeval", 2, 2, "Real Real",
  "(Real _handler, Real maxeval)",
  "Stop when the number of function evaluations exceeds maxeval.",
  BOperClassify::MatrixAlgebra_);
void BDat_set_maxeval::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::set_maxeval] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  int maxeval = (int)Real(Arg(2));
  contens_ = instance!=NULL;
  if(instance) { 
    try {
    instance->opt_.set_maxeval(maxeval); 
    } CATCH_NLOPT(contens_);
  }
};


//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_set_maxtime);
DefMethod(1, BDat_set_maxtime, 
  "set_maxtime", 2, 2, "Real Real",
  "(Real _handler, Real maxtime)",
  "Stop when the optimization time (in seconds) exceeds maxtime.",
  BOperClassify::MatrixAlgebra_);
void BDat_set_maxtime::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::set_maxtime] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  int maxtime = (int)Real(Arg(2));
  contens_ = instance!=NULL;
  if(instance) { 
    try {
    instance->opt_.set_maxtime(maxtime); 
    } CATCH_NLOPT(contens_);
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_get_stopping_criterium);
DefMethod(1, BDat_get_stopping_criterium, 
  "get_stopping_criterium", 2, 2, "Real Real",
  "(Real _handler, Real stopping_criterium)",
  "Returns the current value of the parameter defining an stopping "
  "criterium selected among valid values :\n"
  "Set NonLinGloOpt::StoppingCriterium = \n"
  "[[\n"
  "  Real STOPVAL  = 1,\n"
  "  Real FTOL_REL = 2,\n"
  "  Real FTOL_ABS = 3,\n"
  "  Real XTOL_REL = 4,\n"
  "  Real MAXEVAL  = 5,\n"
  "  Real MAXTIME  = 6\n"
  "]];\n",
  BOperClassify::MatrixAlgebra_);
void BDat_get_stopping_criterium::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::get_stopping_criterium] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  short int stp_crt = (short int)Real(Arg(2));
  contens_ = instance!=NULL;
  try {
    if(instance) { switch (stp_crt) {
      case 1 : contens_ = instance->opt_.get_stopval(); break;
      case 2 : contens_ = instance->opt_.get_ftol_rel(); break;
      case 3 : contens_ = instance->opt_.get_ftol_abs(); break;
      case 4 : contens_ = instance->opt_.get_xtol_rel(); break;
      case 5 : contens_ = instance->opt_.get_maxeval(); break;
      case 6 : contens_ = instance->opt_.get_maxtime(); break;
    }}
  } CATCH_NLOPT(contens_);
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_optimize);
DefMethod(1, BDat_optimize, 
  "optimize", 3, 4, "Real Matrix Real Real",
  "(Real _handler, Matrix x, Real opt_f [, Real verboseEach])",
  "Once all of the desired optimization parameters have been specified "
  "in a given object opt, you can perform the optimization by calling "
  "this function.\n"
  "On input, x is a vector of length n (the dimension of the problem "
  "from the constructor) giving an initial guess for the optimization "
  "parameters. On successful return, x contains the optimized values "
  "of the optimization parameters, and opt_f contains the corresponding "
  "value of the objective function.\n"
  "The return value is positive on success, indicating the reason for "
  "termination:\n"
  "  SUCCESS = 1 /* generic success code */\n"
  "  STOPVAL_REACHED = 2\n"
  "  FTOL_REACHED = 3\n"
  "  XTOL_REACHED = 4\n"
  "  MAXEVAL_REACHED = 5 \n"
  "  MAXTIME_REACHED = 6\n"
  "On failure returns a negative or null code:\n"
  "  NON_EXISTENT_PROBLEM = 0 /* _handler is bad defined */\n"
  "  FAILURE = -1 /* generic failure code */\n"
  "  INVALID_ARGS = -2\n"
  "  OUT_OF_MEMORY = -3\n"
  "  ROUNDOFF_LIMITED = -4\n"
  "  FORCED_STOP = -5\n",
  BOperClassify::MatrixAlgebra_);
void BDat_optimize::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::optimize] ";
  BDat& _handler = Dat(Arg(1));
  TolNLopt* instance = decode_addr(_handler);
  BMat& X = Mat(Arg(2));
  std::vector<double> x;
  BMat2Vector(X,x);
  double& y = (double&)Dat(Arg(3));
  int verboseEach = 0;
  if(Arg(4)) { verboseEach = (int)Real(Arg(4)); }
  contens_ = 0;

  if(instance) 
  { 
    try 
    {
      int oldVerboseEach = instance->verboseEach_;
      instance->verboseEach_ = verboseEach;
      contens_ = instance->opt_.optimize(x,y); 
      if((instance->target_->numEval_<2) && (contens_>0) &&
         (
          (contens_ == nlopt::SUCCESS)||
          (contens_ == nlopt::FTOL_REACHED)||
          (contens_ == nlopt::XTOL_REACHED)       
         ))
      {
        Error(_MID+"The proccess has failed in the first evaluation for "
              "unknown reasons.");
        contens_ = -1;
      } 
    //Std(_MID+" TRACE y="<<y);
      instance->verboseEach_ = oldVerboseEach;
      Vector2BMat(x,X);
    //Std(_MID+" TRACE X="<<X.Name());
    }
    CATCH_NLOPT(contens_);
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, BDat_eval_target);
DefMethod(1, BDat_eval_target, 
  "eval_target", 2, 3, "Real Matrix Matrix",
  "(Real _handler, Matrix x [, Matrix grad])",
  "Evaluates the target function.",
  BOperClassify::MatrixAlgebra_);
void BDat_eval_target::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::eval_target] ";
  BDat& _handler = Dat(Arg(1));
  BMatrix<double>& x = *(BMatrix<double>*)&Mat(Arg(2));
  BMatrix<double> grad;
  if(Arg(3)) { grad = *(BMatrix<double>*)&Mat(Arg(3)); }
  TolNLopt* instance = decode_addr(_handler);
  if(instance) 
  { 
    TolMethod* met = instance->target_;
    if(met) { contens_ = met->eval(x, grad); }
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, BDat_eval_inequation);
DefMethod(1, BDat_eval_inequation, 
  "eval_inequation", 3, 4, "Real Real Matrix Matrix",
  "(Real _handler, Real k, Matrix x [, Matrix grad])",
  "Evaluates the k-th inequation function.",
  BOperClassify::MatrixAlgebra_);
void BDat_eval_inequation::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::eval_inequation] ";
  BDat& _handler = Dat(Arg(1));
  int k = (int)Real(Arg(2));
  BMatrix<double>& x = *(BMatrix<double>*)&Mat(Arg(3));
  BMatrix<double> grad;
  if(Arg(4)) { grad = *(BMatrix<double>*)&Mat(Arg(4)); }
  TolNLopt* instance = decode_addr(_handler);
  if(instance) 
  { 
    if(k>0 && k<= instance->inequations_.Size())
    {
      TolMethod* met = instance->inequations_[k-1];
      if(met) { contens_ = met->eval(x, grad); }
    }
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMat_eval_inequation_m);
DefMethod(1, BMat_eval_inequation_m, 
  "eval_inequation_m", 3, 4, "Real Real Matrix Matrix",
  "(Real _handler, Real k, Matrix x [, Matrix grad])",
  "Evaluates the k-th multiple inequation function.",
  BOperClassify::MatrixAlgebra_);
void BMat_eval_inequation_m::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::eval_inequation_m] ";
  BDat& _handler = Dat(Arg(1));
  int k = (int)Real(Arg(2));
  BMatrix<double>& x = *(BMatrix<double>*)&Mat(Arg(3));
  BMatrix<double> grad;
  if(Arg(4)) { grad = *(BMatrix<double>*)&Mat(Arg(4)); }
  TolNLopt* instance = decode_addr(_handler);
  if(instance) 
  { 
    if(k>0 && k<= instance->inequations_m_.Size())
    {
      TolMethod* met = instance->inequations_m_[k-1];
      if(met) { Vector2BMat(met->eval_m(x, grad),contens_); }
    }
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_get_evaluation_number);
DefMethod(1, BDat_get_evaluation_number, 
  "get_evaluation_number", 2, 3, "Real Text Real",
  "(Real _handler, Text functionSet [, Real num=1])",
  "Returns the number of times that a function has been evaluated "
  "in an optiization problem. The argument functionSet must be one "
  "of : \"Target\", \"Inequation\", \"Equation\"",
  BOperClassify::MatrixAlgebra_);
void BDat_get_evaluation_number::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::get_stopping_criterium] ";
  BDat& _handler = Dat(Arg(1));
  BText functionSet = Text(Arg(2));
  functionSet.ToLower();
  int num = 0;
  if(Arg(3)) { num = (int)Real(Arg(3)) -1; }
  TolNLopt* instance = decode_addr(_handler);
  //short int stp_crt = (short int)Real(Arg(2));
  contens_ = instance!=NULL;
  try { if(instance) { 
    TolMethod** fs = NULL;
    int size;
    if(functionSet=="target")     
    { 
      fs = &(instance->target_); 
      size = 1;
    }
    if(functionSet=="inequation") 
    { 
      fs = instance->inequations_.GetBuffer();
      size = instance->inequations_.Size();
    }
    if(functionSet=="equation")   
    { 
      fs = instance->equations_.GetBuffer();
      size = instance->inequations_.Size();
    }
    if((num<0)||(num>=size))
    {
      Error(_MID+"Argument 'num="+(num+1)+"' out of range [1,"+size+"]");
    }
    else
    {
      contens_ = fs[num]->numEval_;
    }
  }} CATCH_NLOPT(contens_);
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, BDat_eval_equation);
DefMethod(1, BDat_eval_equation, 
  "eval_equation", 3, 4, "Real Real Matrix Matrix",
  "(Real _handler, Real k, Matrix x [, Matrix grad])",
  "Evaluates the k-th equation function.",
  BOperClassify::MatrixAlgebra_);
void BDat_eval_equation::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::eval_equation] ";
  BDat& _handler = Dat(Arg(1));
  int k = (int)Real(Arg(2));
  BMatrix<double>& x = *(BMatrix<double>*)&Mat(Arg(3));
  BMatrix<double> grad;
  if(Arg(4)) { grad = *(BMatrix<double>*)&Mat(Arg(4)); }
  TolNLopt* instance = decode_addr(_handler);
  if(instance) 
  { 
    if(k>0 && k<= instance->equations_.Size())
    {
      TolMethod* met = instance->equations_[k-1];
      if(met) { contens_ = met->eval(x, grad); }
    }
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_nlopt_srand);
DefMethod(1, BDat_nlopt_srand, 
  "srand", 1, 1, "Real",
  "(Real seed)",
  "Use a deterministic sequence of pseudorandom numbers, i.e. the "
  "same sequence from run to run, for stochastic optimization "
  "algorithms",
  BOperClassify::MatrixAlgebra_);
void BDat_nlopt_srand::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::nlopt_srand] ";
  unsigned long seed = (unsigned long)Real(Arg(1));
  nlopt_srand(seed);
  contens_ = true;
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_nlopt_srand_time);
DefMethod(1, BDat_nlopt_srand_time, 
  "srand_time", 1, 1, "Real",
  "(Real unused)",
  "Reset the random seed based on the system timefor stochastic "
  "optimization algorithms",
  BOperClassify::MatrixAlgebra_);
void BDat_nlopt_srand_time::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[NonLinGloOpt::CppTools::nlopt_srand_time] ";
  nlopt_srand_time();
  contens_ = true;
};

//--------------------------------------------------------------------
DeclareContensClass(BTxt, BTxtTemporary, BTxtNLOPTVersion);
DefMethod(1, BTxtNLOPTVersion, 
  "NLOPT.Version", 1, 1, "Real",
  "(Real unused)",
  "Current used NLOPT version identifier",
  BOperClassify::MatrixAlgebra_);
void BTxtNLOPTVersion::CalcContens()
//--------------------------------------------------------------------
{

  int _nlopt_version_major ;
  int _nlopt_version_minor ;
  int _nlopt_version_bugfix ;

  nlopt::version(
    _nlopt_version_major, 
    _nlopt_version_minor, 
    _nlopt_version_bugfix); 
  contens_ = BText("")+
    _nlopt_version_major+"."+
    _nlopt_version_minor+"."+
    _nlopt_version_bugfix;
};


/*
Funciones sin implementar.

nlopt::algorithm nlopt::opt::get_algorithm() const;
unsigned nlopt::opt::get_dimension() const;
const char *nlopt::opt::get_algorithm_name() const;

void nlopt::opt::get_lower_bounds(std::vector<double> &lb);
void nlopt::opt::get_upper_bounds(std::vector<double> &ub);


void nlopt::opt::set_initial_step(const std::vector<double> &dx);
void nlopt::opt::set_initial_step(double dx);
void nlopt::opt::get_initial_step(const std::vector<double> &x, std::vector<double> &dx) const;
std::vector<double> nlopt::opt::get_initial_step(const std::vector<double> &x) const;

void nlopt::opt::set_population(unsigned pop);
unsigned nlopt::opt::get_population() const;

*/

/* * /

//Ejemplo de uso
//http://ab-initio.mit.edu/wiki/index.php/NLopt_Tutorial#Example_in_C.2B.2B
typedef struct {
    double a, b;
} my_constraint_data;

double myvconstraint(const std::vector<double> &x, std::vector<double> &grad, void *data)
{
    my_constraint_data *d = reinterpret_cast<my_constraint_data*>(data);
    double a = d->a, b = d->b;
    if (!grad.empty()) {
        grad[0] = 3 * a * (a*x[0] + b) * (a*x[0] + b);
        grad[1] = -1.0;
    }
    return ((a*x[0] + b) * (a*x[0] + b) * (a*x[0] + b) - x[1]);
}
double myvfunc(const std::vector<double> &x, std::vector<double> &grad, void *my_func_data)
{
    if (!grad.empty()) {
        grad[0] = 0.0;
        grad[1] = 0.5 / sqrt(x[1]);
    }
    return sqrt(x[1]);
}

void sample()
{ 
  nlopt::opt opt(nlopt::LD_MMA, 2);
  std::vector<double> lb(2);
  lb[0] = -HUGE_VAL; lb[1] = 0;
  opt.set_lower_bounds(lb);


  my_constraint_data data[2] = { {2,0}, {-1,1} };
  opt.set_min_objective(myvfunc, NULL);
  opt.add_inequality_constraint(myvconstraint, &data[0], 1e-8);
  opt.add_inequality_constraint(myvconstraint, &data[1], 1e-8);

  opt.set_xtol_rel(1e-4);

  std::vector<double> x(2);
  x[0] = 1.234; x[1] = 5.678;
  double minf;
  nlopt::result result = opt.optimize(x, minf);
}

class my_inequation
{
public:
  double a_;
  double b_;
  my_inequation(double a, double b) : a_(a), b_(b) {};
 ~my_inequation() {};
  double constraint(double x1, double x2)
  {
    double ax1b = a_*x1 + b_;
    return((pow(ax1b,3) - x2));
  }
};


my_inequation ineq_2( 0, 2);

/* */

