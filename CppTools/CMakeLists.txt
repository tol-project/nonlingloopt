# look for tol
find_package( TOL REQUIRED )
if( TOL_FOUND )
  message( STATUS "TOL_INCLUDE_DIR = ${TOL_INCLUDE_DIR}" )
  message( STATUS "TOL_LIBRARIES = ${TOL_LIBRARIES}" )
else( TOL_FOUND )
endif( TOL_FOUND )

find_package(GSL REQUIRED)
if (GSL_FOUND)
  message("GSL_LIBRARIES = ${GSL_LIBRARIES}")
  message("GSL_LIBRARY = ${GSL_LIBRARY}")
  message("GSL_INCLUDE_DIR = ${GSL_INCLUDE_DIR}")
endif(GSL_FOUND)

find_package( CintHeaders )
if( CINT_HEADERS_FOUND )
  message( "CINT_INCLUDE_DIR = ${CINT_INCLUDE_DIR}" )
endif( )

if( MINGW )
  set( plat_base "MINGW" )
elseif( MSVC )
  set( plat_base "MSVC_32" )
elseif( CMAKE_SYSTEM_NAME STREQUAL "Linux" )
  set( plat_base "LINUX" )
else( CMAKE_SYSTEM_NAME STREQUAL "Linux" )
  message( FATAL_ERROR "Could not compile for platform ${CMAKE_SYSTEM}" )
endif( MINGW )

if ( CMAKE_SIZEOF_VOID_P EQUAL 8 )
  set( suf_size "64" )
elseif( CMAKE_SIZEOF_VOID_P EQUAL 4 )
  set( suf_size "32" )
else( CMAKE_SIZEOF_VOID_P EQUAL 4 )
  message( FATAL_ERROR "Unknown pointer size '${CMAKE_SIZEOF_VOID_P}'" )
endif( CMAKE_SIZEOF_VOID_P EQUAL 8 )

set( devel_plat "${plat_base}_${suf_size}" )

set( NLOPT_DIR "${CMAKE_CURRENT_SOURCE_DIR}/nlopt/${devel_plat}" )
message( STATUS "NLOPT_DIR = ${NLOPT_DIR}" )
find_path( NLOPT_INCLUDE_DIR nlopt.h
  PATHS ${NLOPT_DIR}/include
  DOC "Path to NLOPT include"
  NO_DEFAULT_PATH )

if( NLOPT_INCLUDE_DIR )
  find_library(NLOPT_LIBRARY nlopt
    ${NLOPT_DIR}/lib
    NO_DEFAULT_PATH)
  if( NOT NLOPT_LIBRARY )
    message( FATAL_ERROR "could not find library nlopt" )
  endif( NOT NLOPT_LIBRARY )
else( NLOPT_INCLUDE_DIR )
    message( FATAL_ERROR "could not find header file nlopt.h" )
endif( NLOPT_INCLUDE_DIR )

include_directories( ${TOL_INCLUDE_DIR} ${NLOPT_INCLUDE_DIR} ${GSL_INCLUDE_DIR} )

add_subdirectory( source )