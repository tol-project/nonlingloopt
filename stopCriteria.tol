///////////////////////////////////////////////////////////////////////////////
// FILE   : stopCriteria.tol
// PURPOSE: Class NonLinGloOpt::@StopCriteria
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.Scheme = "Defines stop criteria for an optimization "
"method.";
Class @StopCriteria
///////////////////////////////////////////////////////////////////////////////
{
//Los criterios de parada dependerán del motor pero los conceptos son bastante generales.
//Los que se definan como omitido no se aplican.

  Static Real defaultRelTolerance = 1.E-8;  

  //Absolute tolerance for target
  Real absoluteTolerance_target = ?;
  //Relative tolerance for target
  Real relativeTolerance_target = @StopCriteria::defaultRelTolerance;
  //Absolute tolerance for variables
  Matrix absoluteTolerance_var = Rand(0,0,0,0);
  //Relative tolerance for variables
  Real relativeTolerance_var = @StopCriteria::defaultRelTolerance;
  //Maximum run time for the stopping criteria
  Real maxTime = ?;
  //Stop when the number of function evaluations exceeds maxEval
  Real maxEval = ?;
  //Stop when an objective value of at least stopVal is found
  Real stopVal = ?;
  
  Static @StopCriteria Default(Real relTol)
  {
    Real relTol_ = If(IsUnknown(relTol),
      @StopCriteria::defaultRelTolerance,relTol);
    @StopCriteria default = [[
      Real relativeTolerance_target = relTol_;
      Real relativeTolerance_var = relTol_ 
    ]]
  };

  Real apply(@Engine engine)
  {
    Real If(!IsUnknown(relativeTolerance_target), {
      engine::set_ftol_rel(relativeTolerance_target) });
    Real If(!IsUnknown(relativeTolerance_var), {
      engine::set_xtol_rel(relativeTolerance_var) });
    Real If(!IsUnknown(absoluteTolerance_target), {
      engine::set_ftol_abs(absoluteTolerance_target) });
    Real If(Rows(absoluteTolerance_var), {
      engine::set_xtol_abs(absoluteTolerance_var)});
    Real If(!IsUnknown(maxTime), { 
      engine::set_maxtime(maxTime) });
    Real If(!IsUnknown(maxEval), { 
      engine::set_maxeval(maxEval) });
    Real If(!IsUnknown(stopVal), { 
      engine::set_stopval(stopVal) });
    True
  };

  /////////////////////////////////////////////////////////////////////////////
  Text info(Text margin)
  /////////////////////////////////////////////////////////////////////////////
  {
    margin+"Stop criteria:\n"+
    margin+margin+"Absolute tolerance for target: "<<absoluteTolerance_target+"\n"+
    margin+margin+"Relative tolerance for target: "<<relativeTolerance_target+"\n"+
    margin+margin+"Absolute tolerance for variables: "<<absoluteTolerance_var+"\n"+
    margin+margin+"Relative tolerance for variables: "<<relativeTolerance_var+"\n"+
    margin+margin+"Maximum running time: "<<maxTime+"\n"+
    margin+margin+"Maximum number of evaluations: "<<maxEval+"\n"+
    margin+margin+"Sufficient target value: "<<stopVal+"\n"
  }

};
