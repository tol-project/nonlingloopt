///////////////////////////////////////////////////////////////////////////////
// FILE   : linInqMinResSol.tol
// PURPOSE: Utilidades para soluciones de m�nimos residuos con restricciones
// de desigualdad lineal      
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.LinInqMinResSol = "Solves the constrained minimum "
"residuals problem as a symmetric quadratic program:\n"
"min { e' e }"
"Subject to \n"
" e = y-Xb\n"
" A b <= a\n"
"In order to get a better performance, domain constrains will be define "
"outside inequations as lower and upper bounds:\n "
" L <= x <= U \n";
Class @LinInqMinResSol : @QuadProg.Base {
///////////////////////////////////////////////////////////////////////////////
//Input and output matrices are stored as references by means of a 
//Set of VMatrix, in order to  avoid copies of large objects
Set _.X;
Set _.y;

Real _.m = ?;
Real _.mInv = ?;

////////////////////////////////////////////////////////////////////////////////
//Target function defined as simple code
Real original.target(Matrix x, Matrix grad)
////////////////////////////////////////////////////////////////////////////////
{
//WriteLn("TRACE [@LinInqMinResSol::original.target] 1 x = \n"<<Matrix Tra(x));
//WriteLn("TRACE [@LinInqMinResSol::original.target] 2 grad:"<<Rows(grad)+"x"<<Columns(grad));
  VMatrix b = Mat2VMat(x);
  VMatrix Xb = _.X[1]*b;
//WriteLn("TRACE [@LinInqMinResSol::original.target] 3 Xb = \n"<<Matrix VMat2Mat(Xb,1));
  VMatrix e = _.y[1]-Xb;
//WriteLn("TRACE [@LinInqMinResSol::original.target] 4 y = \n"<<Matrix VMat2Mat(Xb,1));
  Matrix If(Rows(grad),
  {
     grad := -VMat2Mat((Tra(e)*_.X[1])*(2*_.mInv),True)
  });
  Real f = VMatSum(e$*e)*_.mInv;
//WriteLn("TRACE [@LinInqMinResSol::original.target] 5 y = \n"<<y);
//WriteLn("TRACE [@LinInqMinResSol::original.target] 6 grad = \n"<<Matrix Tra(grad));
  f
};

////////////////////////////////////////////////////////////////////////////////
Real _check.linInqMinResSol(Real void)
////////////////////////////////////////////////////////////////////////////////
{
//WriteLn("TRACE [@LinInqMinResSol::setup] 1");
  Real If(!_check.quadProg.base(void), False, {
  Set aref = [[_this]]; 
  Real _.m := VRows(_.X[1]);
//WriteLn("TRACE [@LinInqMinResSol::setup] 2 _.m="<<_.m);
  Real _.mInv := 1/_.m;
//WriteLn("TRACE [@LinInqMinResSol::setup] 3 _.mInv="<<_.mInv);
  Real cX = VColumns(_.X[1]);
  Real cy = VColumns(_.y[1]);
  Real ry = VRows(_.y[1]);
  Text msg = "";
  Text If(cX!=_.n, msg := msg + 
    "Input matrix should have just "<<_.n+" columns instead of "<<cX+"\n");
  Text If(cy!=1, msg := msg + 
    "Output matrix should have just one column instead of "<<cy+"\n");
  Text If(cy!=1, msg := msg + 
    "Output matrix should have just "<<_.m+" columns instead of "<<ry+"\n");
  If(msg=="",True, { Error(msg); False })
})};


////////////////////////////////////////////////////////////////////////////////
Real check(Real void)
////////////////////////////////////////////////////////////////////////////////
{
  _check.linInqMinResSol(void)
};

////////////////////////////////////////////////////////////////////////////
Static @LinInqMinResSol Bounded(
  VMatrix X, 
  VMatrix y,
  VMatrix A, 
  VMatrix a, 
  VMatrix L, 
  VMatrix U, 
  NonLinGloOpt::@Polytope.Config config_)
////////////////////////////////////////////////////////////////////////////
{
  @LinInqMinResSol new = [[
    Real _.n = VColumns(X); 
    Set _.X = [[X]];
    Set _.y = [[y]];
    VMatrix _.A = A;
    VMatrix _.a = a;
    VMatrix _.lowerBounds = L;
    VMatrix _.upperBounds = U;
    NameBlock config = config_ ]];
//WriteLn("TRACE [@LinInqMinResSol::New] 1 _.A:("<<VRows(new::_.A)+"x"<<VColumns(new::_.A)+")");
//WriteLn("TRACE [@LinInqMinResSol::New] 2 _.a:("<<VRows(new::_.a)+"x"<<VColumns(new::_.a)+")");
//WriteLn("TRACE [@LinInqMinResSol::New] 3 _.X:"<<Name(new::_.X[1])+":("<<VRows(new::_.X[1])+"x"<<VColumns(new::_.X[1])+")");
//WriteLn("TRACE [@LinInqMinResSol::New] 4 _.y:"<<Name(new::_.y[1])+":("<<VRows(new::_.y[1])+"x"<<VColumns(new::_.y[1])+")");
  new
};

////////////////////////////////////////////////////////////////////////////
Static @LinInqMinResSol Unbounded(
  VMatrix X, 
  VMatrix y,
  VMatrix A, 
  VMatrix a, 
  NonLinGloOpt::@Polytope.Config config_)
////////////////////////////////////////////////////////////////////////////
{
  @LinInqMinResSol new = [[
    Real _.n = VColumns(X); 
    Set _.X = [[X]];
    Set _.y = [[y]];
    VMatrix _.A = A;
    VMatrix _.a = a;
    NameBlock config = config_ ]];
//WriteLn("TRACE [@LinInqMinResSol::New] 1 _.A:("<<VRows(new::_.A)+"x"<<VColumns(new::_.A)+")");
//WriteLn("TRACE [@LinInqMinResSol::New] 2 _.a:("<<VRows(new::_.a)+"x"<<VColumns(new::_.a)+")");
//WriteLn("TRACE [@LinInqMinResSol::New] 3 _.X:"<<Name(new::_.X[1])+":("<<VRows(new::_.X[1])+"x"<<VColumns(new::_.X[1])+")");
//WriteLn("TRACE [@LinInqMinResSol::New] 4 _.y:"<<Name(new::_.y[1])+":("<<VRows(new::_.y[1])+"x"<<VColumns(new::_.y[1])+")");
  new
};

////////////////////////////////////////////////////////////////////////////
Static VMatrix Solve(
  VMatrix X, 
  VMatrix y,
  VMatrix A, 
  VMatrix a, 
  NonLinGloOpt::@Polytope.Config config_ )
////////////////////////////////////////////////////////////////////////////
{
  @LinInqMinResSol new = 
    NonLinGloOpt::@LinInqMinResSol::Unbounded(X,y,A,a,config_);
  new::solve(?)
}

};

