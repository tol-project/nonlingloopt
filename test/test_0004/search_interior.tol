////////////////////////////////////////////////////////////////////////////////
//Example of use of package NonLinGloOpt
// 
//The problem is defined in the tutorial of NLopt
//  
//  http://ab-initio.mit.edu/wiki/index.php/NLopt_Tutorial
// 
////////////////////////////////////////////////////////////////////////////////

//Don�t forget to load the package if it has not yet been loaded
#Require NonLinGloOpt;

Real nObject.begin = Copy(NObject);
WriteLn("TRACE 1 nObject.begin="<<nObject.begin);


////////////////////////////////////////////////////////////////////////////
NameBlock BuildInequationHandler(VMatrix A_, VMatrix a_)
////////////////////////////////////////////////////////////////////////////
{[[
  Set _.A = [[A_]]; 
  Set _.a = [[a_]];
  Real _.r = VRows(A_);
  Set _.A_row = Copy(Empty);
  Set _.a_row = Copy(Empty);
  Set _.inequations = Copy(Empty);

  ////////////////////////////////////////////////////////////////////////////
  Real linear_inequation(Matrix b_, Matrix grad, NameBlock additionalData)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix b = Mat2VMat(b_);
    Real i = additionalData::linearConstrainingRow;
    Matrix If(Rows(grad),
    {
      grad := VMat2Mat(_.A_row[i],True)
    });
    VMatDat(_.A_row[i]*b,1,1) - _.a_row[i]
  };

  ////////////////////////////////////////////////////////////////////////////
  Real build_linear_inequations(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set aref = [[_this]]; 
    Set _.inequations := For(1,_.r,Set build_linear_inequation(Real i)
    {[[
      NameBlock aref[1], 
      Code linear_inequation, 
      {
        NameBlock auxData = [[ Real linearConstrainingRow = i ]];
        Eval("NameBlock auxData_"<<i+"=auxData")
      }
    ]]});
    Set _.A_row := For(1,_.r, VMatrix build_A_row (Real i) 
    { SubRow(_.A[1],[[i]]) });
    Set _.a_row := For(1,_.r, Real   buidl_a_row (Real i) 
    { VMatDat(_.a[1],i,1) });
    True
  }
]]};


////////////////////////////////////////////////////////////////////////////
//Maximum Likelihood and Interior Border Distance
//VMatrix FindFeasible.MLIBD(VMatrix A_, VMatrix a_, 
//                         VMatrix X_, VMatrix Y_, Real verbose)
////////////////////////////////////////////////////////////////////////////
//{


  Real verbose = True;

  Real r_ = VRows(A_);
  Real ri_ = 1/r_;
  Real n_ = VColumns(X_);
  Real N_ = VRows(X_);
  Real Ni_ = 1/N_;


  NameBlock ineq = BuildInequationHandler(A_,a_);
  Real ineq::build_linear_inequations(?);


  ////////////////////////////////////////////////////////////////////////////////
  //Target function defined as simple code
  Real inequation_D3(Matrix b_, Matrix grad)
  ////////////////////////////////////////////////////////////////////////////////
  {
    VMatrix b = Mat2VMat(b_);
    VMatrix g = A_*b-a_;
    VMatrix gm = g$*GE(g,g*0);
    VMatrix gm2 = gm$*gm;
    Real gDist = VMatSum(gm2);
    Real If(Rows(grad),
    {
      Matrix grad := VMat2Mat(Tra(gm)*A_*2,True);
      True
    });
    gDist 

  };

  ////////////////////////////////////////////////////////////////////////////////
  //Target function defined as simple code
  Real Target(Matrix b_, Matrix grad)
  ////////////////////////////////////////////////////////////////////////////////
  {
    VMatrix b = Mat2VMat(b_);
    VMatrix e = Y_-X_*b;
    Real eDist = VMatAvr(e$*e);
    Real If(Rows(grad),
    {
      Matrix grad := 
        -VMat2Mat(Tra(e)*X_,True)*2*Ni_;
      True
    });
    eDist
  };

  NonLinGloOpt::@PipeLine pipe_line = [[
    //Defining the problem
    Set problems = [[ NonLinGloOpt::@Problem problem = [[
      //We want to minimize the target function
      Real sign = -1;
      //We want just a local minimum
      Real neededGlobal = False;
      //The problem is almost twice differentiable
      Real id_analyticalClass = 
        NonLinGloOpt::AnalyticalClass::TWICE_DIFFERENTIABLE;
      //The gradient of all functions is known 
      Real id_useGradient = False;
      //Problem dimension
      Real n = n_;
      //Target given as simple Code
      Anything target = Target;
      Set inequations = ineq::_.inequations;
    //Set inequations =  [[inequation_D3]];

      //Lower bounds 
      Matrix lower_bounds = VMat2Mat(modDef::_.lb);
      //Upper bounds 
      Matrix upper_bounds = VMat2Mat(modDef::_.ub);
      Real inequationTolerance = 1.E-10
    ]] ]];
    //Initial point
    Real verboseEach = If(verbose, 100, 0)
  ]];
//WriteLn("TRACE [FindFeasible.MLE] pipe_line::n="<<((pipe_line::problems)[1])::n);
//WriteLn("TRACE [FindFeasible.MLE] lower_bounds=\n"<<Matrix Tra(((pipe_line::problems)[1])::lower_bounds));
//WriteLn("TRACE [FindFeasible.MLE] pper_bounds=\n"<<Matrix Tra(((pipe_line::problems)[1])::upper_bounds));
  Real { pipe_line::add_method( NonLinGloOpt::@Method local = [[
    Real id_algorithm = NonLinGloOpt::Algorithm::LN_COBYLA,
    NonLinGloOpt::@StopCriteria stopCriteria = [[
      Real relativeTolerance_target = 1.E-10; 
      Real relativeTolerance_var = 1.E-12
    ]]
  ]]) };
  Real pipe_line::optimize(True);
  WriteLn("TRACE pipe_line::x.opt=\n"<<Matrix Tra(pipe_line::x.opt));
//VMatrix Mat2VMat(pipe_line::x.opt)
//};

/* */

Real pipe_line::optimize(True);
VMatrix beta.opt = Mat2VMat(pipe_line::x.opt);
VMatrix residuals.opt =Y_-X_*beta.opt;
VMatrix g = modDef::A*beta.opt-modDef::a;
Real G = VMatMax(g);
Real is_feasible = LE(G,0);

VMatrix beta.mrs = Mat2VMat(MinimumResidualsSolve(VMat2Mat(X_),VMat2Mat(Y_)));
VMatrix residuals.mrs =Y_-X_*beta.mrs;

Real sigma.real = Sqrt(VMatAvr(modDef::e^2));
Real sigma.mrs = Sqrt(VMatAvr(residuals.mrs^2));
Real sigma.opt = Sqrt(VMatAvr(residuals.opt^2));

VMatrix beta.cmp = modDef::b | beta.mrs | beta.opt;

Real nObject.end = Copy(NObject);
WriteLn("TRACE 2 nObject.end="<<nObject.end);
/* */
