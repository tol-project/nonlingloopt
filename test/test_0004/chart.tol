VMatrix M = MtMSqr(modDef::X);
Real M11 = VMatDat(M,1,1);
Real M12 = VMatDat(M,1,2);
Real M22 = VMatDat(M,2,2);
Real b1 = VMatDat(beta.mrs,1,1);
Real b2 = VMatDat(beta.mrs,2,1);
Text SGN(Real a)
{
  If(a<0,"-","+")<<Real Abs(a)
};
Text gamma1 = "(x"<<SGN(-b1)+")";
Text gamma2 = "(y"<<SGN(-b2)+")";
Real ratio_base = If(!ObjectExist("VMatrix","beta.opt"),3,
{
  VMatrix z = beta.opt-beta.mrs;
  VMatDat(Tra(z)*M*z,1,1)^(1/3)
});

Text IsoDensityCurve(Real num)
{"
[Relation"<<(num+2)+"]
Relation = "<<M11+"*"+gamma1+"^2 "<<SGN(M22)+"*"+gamma2+"^2 "
            <<SGN(2*M12)+"*"+gamma1+"*"+gamma2+" = "
            <<(ratio_base^num)+"
Style = 0
Color = clLime
LegendText = Iso-density curve "<<num+"

"};

Text WriteFile("chart.grf",
"
;This file was created by Graph (http://www.padowan.dk)
;Do not change this file from other programs.
[Graph]
Version = 4.3.0.384
MinVersion = 2.5
OS = Windows NT 6.1 

[Axes]
AxesColor = clBlue
GridColor = 0x00FF9999
ShowLegend = 1
Radian = 1

[Relation1]
Relation = "<<VMatDat(modDef::A,1,1)+" x "<<SGN(VMatDat(modDef::A,1,2))+" y <= "<<VMatDat(modDef::a,1,1)+" 
Constraints = 1>0 "+
SetSum(For(2,modDef::r,Text(Real k)
{
 " and "<<VMatDat(modDef::A,k,1)+" x "<<SGN(VMatDat(modDef::A,k,2))+" y <= "<<VMatDat(modDef::a,k,1)
}))+
" and x >= "<<VMatDat(modDef::_.lb,1,1)+
" and x <= "<<VMatDat(modDef::_.ub,1,1)+
" and y >= "<<VMatDat(modDef::_.lb,2,1)+
" and y <= "<<VMatDat(modDef::_.ub,2,1)+
"
Style = 7
Color = clSilver
LegendText = Feasible region

[Relation2]
Relation = "<<VMatDat(modDef::A,1,1)+" x "<<SGN(VMatDat(modDef::A,1,2))+" y <= "<<(VMatDat(limrs::_.aInt,1,1)) +"
Constraints = 1>0 "+
SetSum(For(2,modDef::r,Text(Real k)
{
 " and "<<VMatDat(modDef::A,k,1)+" x "<<SGN(VMatDat(modDef::A,k,2))+" y <= "<<(VMatDat(limrs::_.aInt,k,1))
}))+
" and x >= "<<VMatDat(modDef::_.lb,1,1)+
" and x <= "<<VMatDat(modDef::_.ub,1,1)+
" and y >= "<<VMatDat(modDef::_.lb,2,1)+
" and y <= "<<VMatDat(modDef::_.ub,2,1)+
"
Style = 6
Color = clYellow
LegendText = Forced interior region

[PointSeries1]
FillColor = clLime
LineColor = clLime
Size = 4
Style = 6
LabelPosition = 1
Points = "<<VMatDat(beta.mrs,1,1)+","<<VMatDat(beta.mrs,2,1)+";
LegendText = Unconstrained Optimum
"+
If(!ObjectExist("VMatrix","beta.opt"),"",
"
[PointSeries2]
FillColor = clRed
LineColor = clRed
Size = 4
Style = 6
LabelPosition = 2
Points = "<<VMatDat(beta.opt,1,1)+","<<VMatDat(beta.opt,2,1)+";
LegendText = Constrained Optimum
")+
"
"+IsoDensityCurve(1)+"
"+IsoDensityCurve(2)+"
"+IsoDensityCurve(3)+"
"+IsoDensityCurve(4)+"


[Data]
TextLabelCount = 0
FuncCount = 0
PointSeriesCount = 0
ShadeCount = 0
RelationCount = 1
OleObjectCount = 0

");

